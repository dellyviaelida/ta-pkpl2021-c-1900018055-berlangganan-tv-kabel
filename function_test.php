<?php
// butuh Framework PHPUnit
use PHPUnit\Framework\TestCase;

// class yang mau di TEST
require_once "function.php";

// class untuk run Testing
class function_test extends TestCase
{
    public function testsubharga()
    {
        // kita pakai class yang mau kita test
        $ht = new Hitung();

        $harga = 25000;
        $jumlah = 4;
        $this->assertEquals(100000, $ht->subharga($harga, $jumlah));
    }
}